<?php

namespace app\controllers;

use Yii;
use yii\web\Response;
use yii\web\Controller;
use app\models\Phones;
use app\models\PhoneForm;

/**
 * Контроллер для работы с телефонным справочником
 */
class PhonesController extends Controller {

	/**
	 * Страница телефонного справочника
	 *
	 * @return string
	 */
	public function actionIndex() {
		$phones = new Phones();

		return $this->render('index', $phones->getList());
	}

	/**
	 * Создание записи в телефонном справочнике
	 *
	 * @return array
	 */
	public function actionCreate() {
		$phoneForm = new PhoneForm();

		if($phoneForm->load(Yii::$app->request->post()) && $phoneForm->validate()) {
			$phones = new Phones();

			$phones->name = $phoneForm->name;
			$phones->phone = $phoneForm->phone;
			$phones->email = $phoneForm->email;
			$phones->save();

			$phoneId = $phones->id;

			if($phoneId !== false) {
				if(Yii::$app->request->isAjax === true) {
					Yii::$app->response->format = Response::FORMAT_JSON;

					return [
						'success' => true,
						'phoneId' => $phoneId,
						'content' => $this->renderPartial('index', $phones->getList()),
					];
				}
			}
		}

		return $this->renderAjax($this->action->id, [
			'phoneForm' => $phoneForm,
		]);
	}

	/**
	 * Изменение записи в телефонном справочнике
	 *
	 * @return array
	 */
	public function actionUpdate() {
		$id = Yii::$app->request->get('id');

		$phones    = new Phones();
		$phoneForm = new PhoneForm();
		$phone     = $phones->getOne($id);

		$phoneForm->name = $phone->name;
		$phoneForm->phone = $phone->phone;
		$phoneForm->email = $phone->email;

		if($phoneForm->load(Yii::$app->request->post()) && $phoneForm->validate()) {
			$phone->name  = $phoneForm->name;
			$phone->phone = $phoneForm->phone;
			$phone->email = $phoneForm->email;
			$phone->save();

			$phoneId = $phone->id;

			if($phoneId !== false) {
				if(Yii::$app->request->isAjax === true) {
					Yii::$app->response->format = Response::FORMAT_JSON;

					return [
						'success' => true,
						'phoneId' => $phoneId,
						'content' => $this->renderPartial('index', $phones->getList()),
					];
				}

			}
		}

		return $this->renderAjax($this->action->id, [
			'phoneForm' => $phoneForm,
		]);
	}

	/**
	 * Удаление записи из телефонного справочника
	 *
	 * @return array
	 */
	public function actionDelete($id = false) {
		Yii::$app->response->format = Response::FORMAT_JSON;

		$phones = Phones::find()->where(['id' => $id])->one();
		$phones->delete();

		return [
			'success' => true,
			'content' => $this->renderPartial('index', $phones->getList()),
		];
	}
}