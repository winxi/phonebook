<?php
/**
 * Форма для изменения записи в телефонном справочнике
 *
 * @var app\models\PhoneForm $phoneForm
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
	'id' => 'update-form'
]) ?>
	<div class="row">
		<?= $form->field($phoneForm, 'name') ?>
	</div>
	<div class="row">
		<?= $form->field($phoneForm, 'phone') ?>
	</div>
	<div class="row">
		<?= $form->field($phoneForm, 'email') ?>
	</div>
	<div class="row">
		<?= Html::submitButton('Сохранить') ?>
	</div>
<?php ActiveForm::end() ?>
