<?php
/**
 * Форма для добавления записи в телефонный справочник
 *
 * @var app\models\PhoneForm $phoneForm
 */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin([
	'id' => 'create-form'
]) ?>
	<div class="row">
		<?= $form->field($phoneForm, 'name') ?>
	</div>
	<div class="row">
		<?= $form->field($phoneForm, 'phone') ?>
	</div>
	<div class="row">
		<?= $form->field($phoneForm, 'email') ?>
	</div>
	<div class="row">
		<?= Html::submitButton('Добавить') ?>
	</div>
<?php ActiveForm::end() ?>