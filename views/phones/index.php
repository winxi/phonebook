<?php
/**
 * Отображение телефонного справочника
 *
 * @var $this yii\web\View
 * @var app\models\Phones $phones       Позиции телефонного справочника
 * @var app\models\Phones $pagination   Пагинация телефонного справочника
 */

use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\helpers\Url;
use yii\bootstrap\Modal;

$this->title = 'Телефонный справочник';
?>
<div class="phonebook">
	<!--<a href="#" class="create-link" data-url="--><?//= Url::to(['/phones/create']) ?><!--">Добавить</a>-->
	<?php Modal::begin([
		'id' => 'modal-create',
		'size' => 'modal-lg',
		'clientOptions' => false,
		'toggleButton' => [
			'label' => 'Добавить',
			'tag' => 'a',
			'data-target' => '#modal-create',
			'href' => Url::to(['/phones/create']),
		]
	]);
	Modal::end() ?>
	<ul>
		<?php foreach($phones as $phone): ?>
			<li>
				<div class="col-lg-4"><?= $phone->name ?></div>
				<div class="col-lg-4"><?= $phone->phone ?></div>
				<div class="col-lg-4"><?= $phone->email ?></div>
				<?php Modal::begin([
					'id' => 'modal-update-' . $phone->id,
					'size' => 'modal-lg',
					'clientOptions' => false,
					'toggleButton' => [
						'label' => 'Редактировать',
						'tag' => 'a',
						'data-target' => '#modal-update-' . $phone->id,
						'href' => Url::to(['/phones/update', 'id' => $phone->id]),
					]
				]);
				Modal::end() ?>
				<!--<a href="#" class="update-link" data-url="--><?//= Url::to(['/phones/update', 'id' => $phone->id]) ?><!--">Редактировать</a>-->
				<a href="#" class="delete-link" data-url="<?= Url::to(['/phones/delete', 'id' => $phone->id]) ?>">Удалить</a>
			</li>
		<? endforeach ?>
	</ul>
	<?= LinkPager::widget(['pagination' => $pagination, 'class' => 'pager']) ?>
</div>