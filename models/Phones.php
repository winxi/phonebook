<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\data\Pagination;

/**
 * Телефонный справочник
 *
 * @property int    $id
 * @property string $name   ФИО
 * @property string $phone  Мобильный номер телефона
 * @property string $email  E-mail
 */
class Phones extends ActiveRecord {

	/** Количество записей на одну страницу */
	const PER_PAGE = 20;

	/**
	 * Получения списка записей телефонного справочника
	 *
	 * @return array
	 */
	public function getList() {
		$query = self::find();

		$pagination = new Pagination([
			'route' => 'phones/index',
			'forcePageParam' => false,
			'defaultPageSize' => self::PER_PAGE,
			'totalCount' => $query->count()
		]);

		$phones = $query
			->orderBy(['id' => SORT_DESC])
			->offset($pagination->offset)
			->limit($pagination->limit)
			->all();

		return [
			'phones' => $phones,
			'pagination' => $pagination
		];
	}

	/**
	 * Получение одной записи
	 *
	 * @param int $id Идентификатор записи в телефонном справочнике
	 *
	 * @return array|null|ActiveRecord
	 */
	public function getOne($id) {

		$query = self::find();
		$phone = $query
			->where(['id' => $id])
			->one();

		return $phone;
	}

}