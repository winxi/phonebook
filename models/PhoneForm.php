<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Модель формы создания/изменения записи в телефонном справочнике
 */
class PhoneForm extends Model {

	public $name;
	public $phone;
	public $email;

	/**
	 * @inheritdoc
	 *
	 * @return array
	 */
	public function rules() {
		$rules = [
			[['name', 'phone'], 'required', 'message' => 'Заполните поле'],
			['email', 'email', 'message' => 'Неправильный формат {attribute}']
		];

		return $rules;
	}

	/**
	 * @inheritdoc
	 *
	 * @return array
	 */
	public function attributeLabels() {
		return [
			'name'	    => 'ФИО',
			'phone'		=> 'Номер телефона',
			'email'		=> 'E-mail',
		];
	}

}

